# tux.nix

{ config, ... }:

{ 
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.tux = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" ];
  };
}
