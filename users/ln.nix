# ln.nix

{ config, pkgs, ... }:

{ 
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.ln = {
    isNormalUser = true;
    home = "/home/ln";
    description = "LN";
    extraGroups = [ "libvirtd" "networkmanager" "wheel"]; # virtualisation & networkmanager & ‘sudo’ for the user
  };

  environment.systemPackages = with pkgs; [
    gnome3.simple-scan
    gnupg
    kdenlive
    ktimer
    leafpad
    obs-studio
    pandoc
    qtox
    shadowsocks-libev
    speedcrunch
    sublime3
    transmission
    turbovnc
  ];

  programs.gnupg.agent = {
    enable = true;
    pinentryFlavor = "qt";
  };

}

