# vp.nix

{ config, ... }:

{ 
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vlasta = {
    isNormalUser = true;
    home = "/home/vlasta";
    description = "Vlasta";
    extraGroups = [ "networkmanager" "wheel" ];
  };
}

