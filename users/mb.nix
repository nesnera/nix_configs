# mb.nix

{ config, pkgs, ... }:

{ 
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.mirek = {
    isNormalUser = true;
    home = "/home/mirek";
    description = "Mirek";
    extraGroups = [ "networkmanager" "wheel" ];
  };

  environment.systemPackages = with pkgs; [
    wayvnc
  ];

}

