# hj.nix

{ config, pkgs, ... }:

{ 
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.helena = {
    isNormalUser = true;
    home = "/home/helena";
    description = "Helena";
    extraGroups = [ "networkmanager" "wheel" ];
  };

  environment.systemPackages = with pkgs; [
    wayvnc
  ];

}

