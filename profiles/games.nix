# games.nix

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    fish-fillets-ng

    (steam.override {
       withPrimus = true;
       extraPkgs = pkgs: [ bumblebee glxinfo ];
    }).run

    (steam.override { withJava = true; })

  ];

  programs.steam = {
    enable = true;
    # remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    # dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  nixpkgs.config.packageOverrides = pkgs: {
    steam = pkgs.steam.override {
      extraPkgs = pkgs: with pkgs; [
        libgdiplus
      ];
    };
  };

  programs.java.enable = true;
  # hardware.opengl.driSupport32Bit = true;

}


