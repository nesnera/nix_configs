# graphics.nix

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    ffmpeg
    gimp
    imagemagick
    inkscape
    mediainfo
  ];
}
