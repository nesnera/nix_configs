# basic.nix

{ config, pkgs, ... }:

let 
  pkgs-unstable = import (fetchTarball https://github.com/nixos/nixpkgs/archive/nixpkgs-unstable.tar.gz) {}; 
in
{
  imports =
    [
      /etc/nixos/hardware-configuration.nix # hardware scan result
      #../modules/users.nix # Home Manager
    ];

  nixpkgs.config.allowUnfree = true;

  boot.kernelParams = [ "boot.shell_on_fail" ];

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    calibre
    datovka
    element-desktop
    firefox-wayland
    gitAndTools.gitFull
    htop
    chromium
    killall
    libreoffice-fresh
    lynx
    thunderbird-bin
    vim
    vlc
    wget
    yt-dlp
  ];

  time.timeZone = "Europe/Prague";

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  users.extraUsers.root.openssh.authorizedKeys.keys =
  [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsf53aV88APpMi32iAxmD2nB3fe9gl1SY0eCCs05L0HVHUJ1s6M2OBjribOVmVg/5LvglI1EfhXu8NyLOA89v0o59ubCwhCYIjZ5OQiU+6All+W6hicegd3bBhvwl0jcukJuW7knDXa7z2HfKma0ISfdVWwXGP7bAdAOuJp11e2We7Gav1C1+FzKokXqRayJMFlcwMWg9qqRfd0nt+YyO6Ck5KoMSxb/9sxvD5L2eTjbgkVYmXQQOPDq6fR+O79ONVkRmCGW3SpEhkR+fsoTCz289RXjc8dF16gBkq4JIOLi4m/S8FO1lyxM1Y66uKSL8cFjH1v9ojJtXwura+bDYr ln@pb" ];

  # X11
  services.xserver.enable = true;
  services.xserver.layout = "cz,us"; # bez mezer
  services.xserver.xkbOptions = "eurosign:e,grp:ctrls_toggle"; # €, přepínání pomocí obou Ctrl
  
  systemd.services."user@".serviceConfig.TimeoutStopSec = 10; # odstraňuje 01:30 čekání při vypínání a přihlášených uživatelích

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.09"; # Did you rad the comment?

}

