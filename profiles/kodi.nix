# kodi.nix

{ config, pkgs, ... }:

let 
  pkgs-unstable = import (fetchTarball https://github.com/nixos/nixpkgs/archive/nixpkgs-unstable.tar.gz) {}; 
in
{
  environment.systemPackages = with pkgs; [
  # Kodi (=from unstable)
    (pkgs-unstable.kodi.withPackages (p: with p; [ inputstream-adaptive pvr-hdhomerun pvr-hts pvr-iptvsimple ]))
  ];
}

