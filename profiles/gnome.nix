# gnome.nix

{ config, pkgs, ... }:

{ 
  services.gnome.games.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.displayManager.gdm.wayland = true;
  services.xserver.desktopManager.gnome.enable = true;
  services.xserver.displayManager.gdm.autoLogin.delay = 2;
  services.xserver.displayManager.autoLogin.enable = true;

  environment.systemPackages = with pkgs; [
    authenticator
    gnomeExtensions.vertical-overview
  ];

  environment.gnome.excludePackages = [
    pkgs.gnome.epiphany
    pkgs.gnome.gnome-calendar
    pkgs.gnome.gnome-tweaks
    pkgs.gnome.totem
  ];
}

