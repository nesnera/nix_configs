# i18n_en.nix

{ config, ... }:

{
  console = {
    # font = "Lat2-Terminus16";
    # keyMap = "us";
  };

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";

    extraLocaleSettings = {
      LC_CTYPE="cs_CZ.UTF-8";
      LC_NUMERIC="cs_CZ.UTF-8";
      LC_TIME="cs_CZ.UTF-8";
      LC_COLLATE="cs_CZ.UTF-8";
      LC_MONETARY="cs_CZ.UTF-8";
      LC_MESSAGES="en_US.UTF-8";
      LC_PAPER="cs_CZ.UTF-8";
      LC_NAME="cs_CZ.UTF-8";
      LC_ADDRESS="cs_CZ.UTF-8";
      LC_TELEPHONE="cs_CZ.UTF-8";
      LC_MEASUREMENT="cs_CZ.UTF-8";
      LC_IDENTIFICATION="cs_CZ.UTF-8";
    };
    
    supportedLocales = ["cs_CZ.UTF-8/UTF-8" "en_US.UTF-8/UTF-8"];
  };
}

