# basic_home.nix

{ config, pkgs, ... }:

let 
  pkgs-unstable = import (fetchTarball https://github.com/nixos/nixpkgs/archive/nixpkgs-unstable.tar.gz) {}; 
in

{ 
  environment.systemPackages = with pkgs; [

    # Unstable part
    # pkgs-unstable.tartube

  ];
}
