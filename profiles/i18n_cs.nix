# i18n_cs.nix

{ config, ... }:

{
  # Select internationalisation properties.
  i18n = {
    defaultLocale = "cs_CZ.UTF-8";
    supportedLocales = ["cs_CZ.UTF-8/UTF-8" "en_US.UTF-8/UTF-8"];
  };
}
