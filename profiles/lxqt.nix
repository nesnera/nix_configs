# lxqt.nix

{ config, pkgs, ... }:

{ 

  # Enable the LXQt Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.displayManager.gdm.autoLogin.delay = 2;
  services.xserver.displayManager.autoLogin.enable = true;
  # services.xserver.displayManager.gdm.wayland = true; #podle všeho stále není podporován - https://github.com/lxqt/lxqt/wiki/TODO-for-Wayland 
  services.xserver.desktopManager.lxqt.enable = true;

  environment.systemPackages = with pkgs; [
    brightnessctl
    keysmith
    qpdfview
  ];

  environment.lxqt.excludePackages = [
    pkgs.lxqt.qlipper
  ];

}


