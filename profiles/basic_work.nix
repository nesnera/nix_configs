# basic-work.nix

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
#    falkon
    lynx
    opera
    p7zip
    parted
    picocom
    pwgen
    python3
    tcpdump
    whois
  ];
}

