# virt.nix

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # gnome3.dconf # due to virt-managers amnesia #jinak nefunguje upgrade na NixOS 2022-05
    spice-gtk # due to usb redirection
    virt-manager
  ];
  
  virtualisation.libvirtd.enable = true;
}

