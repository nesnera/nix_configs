# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  pkgs-unstable = import (fetchTarball https://github.com/nixos/nixpkgs/archive/nixpkgs-unstable.tar.gz) {};
in
  {
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
      ./modules/users.nix # Home Manager
    ];

  # Povolení nesvobodného SW
  nixpkgs.config.allowUnfree = true;

  #boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelParams = [ "boot.shell_on_fail" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "tp"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "Europe/Prague";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;
  networking.interfaces.wwp0s20u4i6.useDHCP = true;

  networking.networkmanager.enable = true;
  networking.networkmanager.dns = "dnsmasq";
  programs.nm-applet.enable = true;
  
  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_CTYPE="cs_CZ.UTF-8";
    LC_NUMERIC="cs_CZ.UTF-8";
    LC_TIME="cs_CZ.UTF-8";
    LC_COLLATE="cs_CZ.UTF-8";
    LC_MONETARY="cs_CZ.UTF-8";
    LC_MESSAGES="en_US.UTF-8";
    LC_PAPER="cs_CZ.UTF-8";
    LC_NAME="cs_CZ.UTF-8";
    LC_ADDRESS="cs_CZ.UTF-8";
    LC_TELEPHONE="cs_CZ.UTF-8";
    LC_MEASUREMENT="cs_CZ.UTF-8";
    LC_IDENTIFICATION="cs_CZ.UTF-8";
  };
  i18n.supportedLocales = ["cs_CZ.UTF-8/UTF-8" "en_US.UTF-8/UTF-8"];
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Enable the GNOME 3 Desktop Environment.
  # services.xserver.enable = true;
  # services.xserver.displayManager.gdm.enable = true;
  # services.xserver.desktopManager.gnome3.enable = true;
  
  # Enable the LXQt Desktop Environment.
  services.xserver.enable = true;
  services.xserver.displayManager.gdm.enable = true;
  # services.xserver.displayManager.gdm.wayland = true; #podle všeho stále není podporován - https://github.com/lxqt/lxqt/wiki/TODO-for-Wayland 
  services.xserver.desktopManager.lxqt.enable = true;
  # services.xserver.desktopManager.kodi.enable = true;

  # Configure keymap in X11
  services.xserver.layout = "cz,us"; # bez mezer
  services.xserver.xkbOptions = "grp:ctrls_toggle"; # přepínání pomocí obou Ctrl

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable funcontrol.
  services.thinkfan.enable = true;

  systemd.services."user@".serviceConfig.TimeoutStopSec = 10; # odstraňuje 01:30 čekání při vypínání a přihlášených uživatelích 
  # systemd.extraConfig = "DefaultTimeoutStopSec=15s";

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.ln = {
    isNormalUser = true;
    extraGroups = [ "libvirtd" "wheel" ]; # Enable virtualisation + ‘sudo’ for the user.
  };
  users.users.tux = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    calibre datovka element-desktop falkon git lynx modem-manager-gui signal-desktop tartube thunderbird transmission # klienti
    gimp inkscape leafpad libreoffice-qt obs-studio sublime3 vim # editory
    brightnessctl geteltorito gnome3.simple-scan htop inetutils killall ktimer libcec lm_sensors minio-client ncdu parted p7zip pwgen picocom pinentry_qt5 python3 qpdfview shadowsocks-libev speedcrunch usbutils vlc wget whois # pomůcky bez konfigurace

    virt-manager 
    gnome3.dconf # due to virt-managers amnesia
    spice-gtk # due to usb redirection

    # Unstable part
    pkgs-unstable.youtube-dl
    #   Kodi 19
    (pkgs-unstable.kodi.withPackages (p: with p; [ inputstream-adaptive pvr-hdhomerun pvr-hts pvr-iptvsimple ]))
 
    ];
 
   # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  users.extraUsers.root.openssh.authorizedKeys.keys =
  [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsf53aV88APpMi32iAxmD2nB3fe9gl1SY0eCCs05L0HVHUJ1s6M2OBjribOVmVg/5LvglI1EfhXu8NyLOA89v0o59ubCwhCYIjZ5OQiU+6All+W6hicegd3bBhvwl0jcukJuW7knDXa7z2HfKma0ISfdVWwXGP7bAdAOuJp11e2We7Gav1C1+FzKokXqRayJMFlcwMWg9qqRfd0nt+YyO6Ck5KoMSxb/9sxvD5L2eTjbgkVYmXQQOPDq6fR+O79ONVkRmCGW3SpEhkR+fsoTCz289RXjc8dF16gBkq4JIOLi4m/S8FO1lyxM1Y66uKSL8cFjH1v9ojJtXwura+bDYr ln@pb" ];

  programs.ssh.startAgent = true;

  virtualisation.libvirtd.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

