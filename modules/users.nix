# users.nix

{ config, lib, pkgs, ... }:
let
  home-manager-src = builtins.fetchTarball {
    url = "https://github.com/nix-community/home-manager/archive/209566c752c4428c7692c134731971193f06b37c.tar.gz";
    sha256 = "1canlfkm09ssbgm3hq0kb9d86bdh84jhidxv75g98zq5wgadk7jm";
  };

  nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
    inherit pkgs;
  };

  # nur = builtins.fetchTarball {
  #   # Get the revision by choosing a version from https://github.com/nix-community/NUR/commits/master
  #   url = "https://github.com/nix-community/NUR/archive/152ada262e47963da14f479f5fea9db67e8b4efe.tar.gz"; #2021-03-01
  #   # Get the hash by running `nix-prefetch-url --unpack <url>` on the above url
  #   sha256 = "10shhms0ynxamph13kchjk1gmssf423rg3ipwlac9j6pvygi0r26";
  # };
in
{
  imports = [
    "${home-manager-src}/nixos"
  ];

  environment.systemPackages = with pkgs; [
    gnupg
    pidgin
    pinentry
  ];

  home-manager.users.ln = {
    #imports = lib.attrValues nur.repos.moredhel.hmModules.modules;
    
    home = {
      sessionVariables = {
        EDITOR = "vim";
      };

      #KTimer
      file.".config/ktimerrc".text = ''
        [Job0]
        Command[$e]=$HOME/bin/runSingingBowl
        Consecutive=false
        Delay=3060
        Loop=false
        OneInstance=true
        State=0
        
        [Job1]
        Command[$e]=$HOME/bin/runSingingBowl
        Consecutive=false
        Delay=1980
        Loop=false
        OneInstance=true
        State=0
        
        [Job2]
        Command[$e]=$HOME/bin/runSingingBowl
        Consecutive=false
        Delay=1320
        Loop=false
        OneInstance=true
        State=0
        
        [Job3]
        Command[$e]=$HOME/bin/runSingingBowl
        Consecutive=false
        Delay=660
        Loop=false
        OneInstance=true
        State=0
        
        [Job4]
        Command[$e]=$HOME/bin/runSingingBowl
        Consecutive=false
        Delay=300
        Loop=false
        OneInstance=true
        State=0
        
        [Job5]
        Command[$e]=$HOME/bin/runSingingBowl
        Consecutive=false
        Delay=1
        Loop=false
        OneInstance=true
        State=0

        [Jobs]
        Number=6
           
      '';

      # #PCManFM-Qt
      # file.".config/pcmanfm-qt/lxqt/settings.conf".text = ''
      #   [FolderView]
      #   Mode = detailed
      #   SortColumn = mtime
      #   SortOrder = descending
      #   
      #   [System]
      #   Archiver = xarchiver
      #   Terminal = qterminal
      # '';
    };

    programs = {
      firefox = {
        enable = true;
        # extensions = with pkgs.nur.repos.rycee.firefox-addons; [
        #   https-everywhere
        #   privacy-badger
        #   ublock-origin
        # ];
        # profiles = {
        #   myprofile = {
        #     settings = {
        #     "browser.download.panel.shown" = true;
        #     "browser.search.defaultenginename" = "DuckDuckGo";
        #     "browser.search.region" = "CZ";
        #     "browser.search.selectedEngine" = "DuckDuckGo";
        #     "browser.startup.homepage" = "https://nixos.org/manual/nixos/stable/";
        #     };
        #   };
        # };
      };
      
      # git = {
      #   enable = true;
      #   userName = "nesnera@email.cz";
      # };

      chromium = {
        enable = true;
        extensions = [
          "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
        ];
      };

      pidgin = {
        enable = true;
        plugins =  [ pkgs.pidgin-otr pkgs.pidgin-osd pkgs.telegram-purple ];
      };

      ssh = {
        # enable = true;
        # controlMaster = "auto";
        # controlPersist = "1h";
        # matchBlocks = {
        #   "matomo" = {
        #     hostname = "192.168.122.103";
        #     user = "root";
        #     identityFile = "~/.ssh/mesta_deploy";
        #   };
        #   "ucto" = {
        #     hostname = "37.205.14.138";
        #     port = 10822;
        #     user = "root";
        #     identityFile = "~/.ssh/mesta_deploy";
        #   };
        # };
      };
    };

    services = {
      nextcloud-client.enable = true;
      parcellite.enable = true; # clipbord manager
      redshift = {
        enable = true;
        latitude = "50";
        longitude = "15";
        temperature.day = 5000; # https://www.root.cz/clanky/ohrejte-studene-svetlo-sveho-monitoru-a-setrete-oci/
        temperature.night = 2666;
      };
    };
  };

  home-manager.users.root = {
    home = {
      sessionVariables = {
        EDITOR = "vim";
      };
    };
    programs = {
      ssh = {
        enable = true;
        controlMaster = "auto";
        controlPersist = "1h";
        matchBlocks = {
          "matomo" = {
            hostname = "192.168.122.103";
            user = "root";
            identityFile = "~/.ssh/mesta_deploy";
          };
          "ucto" = {
            hostname = "37.205.14.138";
            port = 10822;
            user = "root";
            identityFile = "~/.ssh/mesta_deploy";
          };
        };
      };
    };
  };

  home-manager.users.tux = {
    home = {
      sessionVariables = {
        EDITOR = "vim";
      };

      #PCManFM-Qt
      file.".config/pcmanfm-qt/lxqt/settings.conf".text = ''
        [FolderView]
        Mode = detailed
        SortCaseSensitive=false
        SortColumn=mtime
        SortFolderFirst=true
        SortHiddenLast=false
        SortOrder=descending

        [Places]
        PlacesApplications=true
        PlacesComputer=true
        PlacesDesktop=true
        PlacesHome=true
        PlacesNetwork=true
        PlacesRoot=true
        PlacesTrash=true
        
        [System]
        Archiver=lxqt-archiver
        FallbackIconThemeName=oxygen
        OnlyUserTemplates=false
        SIUnit=false
        SuCommand=lxqt-sudo %s
        TemplateRunApp=false
        TemplateTypeOnce=false
        Terminal=qterminal        
      '';
      #PCManFM-Qt - bookmarks #chtělo by to relativní cesty
      file.".gtk-bookmarks".text = ''
        file:///home/tux/Hudba Hudba
        file:///home/tux/Videa Videa
      '';

    };

    programs = {
      firefox = {
        enable = true;
        extensions = with nur.repos.rycee.firefox-addons; [
          auto-tab-discard
          clearurls
          https-everywhere
          privacy-badger
          ublock-origin
        ];
        profiles = {
          myprofile = {
            settings = {
            "browser.download.panel.shown" = true;
            "browser.search.defaultenginename" = "DuckDuckGo";
            "browser.search.region" = "CZ";
            "browser.search.selectedEngine" = "DuckDuckGo";
            "browser.startup.homepage" = "https://nixos.org/manual/nixos/stable/";
            };
          };
        };
      };

      chromium = {
        enable = true;
        extensions = [
          "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
        ];
      };
    };
  };

}
