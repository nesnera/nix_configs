# E530c.nix

{ config, pkgs, ... }:

{
  imports =
    [ 
      ../profiles/basic.nix
      ../profiles/basic_home.nix
      ../profiles/boot_uefi.nix
      ../profiles/games.nix
      ../profiles/gnome.nix
      ../profiles/i18n_cs.nix
      ../profiles/kodi.nix
      ../profiles/physical.nix
      ../users/hj.nix
    ];

  # necesary for BCM4313 Wireless Network Adapter (proprietary driver), more at https://gitlab.com/-/snippets/2492007
  boot.blacklistedKernelModules = ["b43" "brcmsmac" "bcma"];
  boot.kernelModules = [ "wl" ];
  boot.extraModulePackages = [ config.boot.kernelPackages.broadcom_sta ];

  networking.hostName = "e530c"; # Define your hostname.
  services.xserver.displayManager.autoLogin.user = "helena";

}
