# IdeaPad_110-15IBR.nix

{ config, pkgs, ... }:

{
  imports =
    [ 
      ../profiles/basic.nix
      ../profiles/basic_home.nix
      ../profiles/boot_uefi.nix
      ../profiles/gnome.nix
      ../profiles/i18n_cs.nix
      ../profiles/kodi.nix
      ../profiles/physical.nix
      ../users/mb.nix
    ];

  networking.hostName = "ip110"; # Define your hostname.
  services.xserver.displayManager.autoLogin.user = "mirek";

  environment.systemPackages = with pkgs; [
    # xorg.xmodmap
  ];

  # system.copySystemConfiguration = true;

}

