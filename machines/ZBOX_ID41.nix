# ZBOX_ID41.nix

{ config, pkgs, ... }:

{
  imports =
    [ 
      ../profiles/basic.nix
      ../profiles/basic_home.nix
      ../profiles/boot_bios.nix
      ../profiles/gnome.nix
      ../profiles/i18n_cs.nix
      ../profiles/physical.nix
      ../users/vp.nix
    ];

  networking.hostName = "zbox_id41"; # Define your hostname.
  services.xserver.displayManager.autoLogin.user = "vlasta";

  environment.systemPackages = with pkgs; [
  ];

  # system.copySystemConfiguration = true;

}

