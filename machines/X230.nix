# X230.nix

{ config, pkgs, ... }:

let
  pkgs-unstable = import (fetchTarball https://github.com/nixos/nixpkgs/archive/nixpkgs-unstable.tar.gz) {};
in
  {
  imports =
    [
      #../modules/users.nix # Home Manager
      ../profiles/basic.nix
      ../profiles/basic_home.nix
      ../profiles/basic_work.nix
      ../profiles/boot_uefi.nix
      ../profiles/games.nix
      ../profiles/graphics.nix
      ../profiles/i18n_en.nix
      ../profiles/kodi.nix
      ../profiles/lxqt.nix
      ../profiles/physical.nix
      ../profiles/virt.nix
      ../users/ln.nix
      ../users/tux.nix
    ];

  boot.blacklistedKernelModules = ["bluetooth" "cdc_acm"];
  boot.kernelPackages = pkgs.linuxPackages_latest; # nejnovější jádro (může dělat problémy s ovladači)

  networking.hostName = "tp"; # Define your hostname.
  services.xserver.displayManager.autoLogin.user = "ln";

  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;
  networking.interfaces.wwp0s20u4i6.useDHCP = true;

  networking.networkmanager.enable = true;
  networking.networkmanager.dns = "dnsmasq";
  programs.nm-applet.enable = true;
  
  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable funcontrol.
  services.thinkfan.enable = true;
 
  programs.ssh.startAgent = true;
}
