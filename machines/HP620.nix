# HP620.nix

{ config, pkgs, ... }:

{
  imports =
    [ 
      ../profiles/basic.nix
      ../profiles/basic_home.nix
      ../profiles/boot_bios.nix
      ../profiles/gnome.nix
      ../profiles/i18n_cs.nix
      ../profiles/kodi.nix
      ../profiles/physical.nix
      ../users/vp.nix
    ];

  networking.hostName = "hp620"; # Define your hostname.
  services.xserver.displayManager.autoLogin.user = "vlasta";

  environment.systemPackages = with pkgs; [
    xorg.xmodmap
  ];

  # system.copySystemConfiguration = true;

  # přemapování nefunkčních kláves
  systemd.services.evdevremapkeys-daemon = {
    enable = true;
    description = "evdevremapkeys";
    serviceConfig = {
      ExecStart = "${pkgs.evdevremapkeys}/bin/evdevremapkeys -f /root/evdevremapkeys.yaml";
    };
    wantedBy = [ "multi-user.target" ];
  };
}

